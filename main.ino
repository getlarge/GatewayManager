///////////////////////////////////////////////////////////////////////////////////
//once settings for connection to wifi and server done, call MySensors lib     //
///////////////////////////////////////////////////////////////////////////////////


void presentation() {
  sendSketchInfo(SKETCH_NAME, SKETCH_VERSION);
}

void receiveOtaSignal(int otaSignal) {
  Serial.print(F("OTA signal received: "));
  Serial.println(otaSignal);
  if (otaSignal == 1 ) {
    //return getUpdated();

  }
}

void mqttReconnect(Config &config) {
  // Loop until we're reconnected
  mqttFailCount = 0;
  //  if ( (strcmp((const char*)config.mqttServer, "") == 0) ) {
  //    return configManager(config);
  //  }
  //  if ( (strcmp((const char*)config.mqttPort, "") == 0) ) {
  //    return configManager(config);
  //  }
  while (!_MQTT_client.connected()) {
    ++mqttFailCount;
    aSerial.vvv().p(F("Connecting to mqtt://")).p((const char*)config.mqttServer).p(":").pln(atoi(config.mqttPort));
    if (_MQTT_client.connect(((const char*)config.mqttClient), ((const char*)config.mqttUser), ((const char*)config.mqttPassword))) {
      aSerial.vvv().p(F("Connected to mqtt as : ")).pln((const char*)config.mqttClient);
      aSerial.vvv().p(F("master topic")).pln(mqttTopicIn);
      _MQTT_client.subscribe((const char*)mqttTopicIn);
    } else {
      aSerial.vvv().p(F("Failed mqtt connection : ")).pln(_MQTT_client.state());
      if (mqttFailCount > mqttMaxFailedCount) {
        aSerial.vv().p(mqttMaxFailedCount).pln(F("+ MQTT connection failure --> config mode"));
        return configManager(config);
      }
      delay(reconnectInterval);
    }
  }
}

//void receive() {
//  //message custom, reset signal
//}

void setup() {
  Serial.println();
}

void loop(void) {
  if ( ! executeOnce) {
    executeOnce = true;
    aSerial.v().println(F("====== Loop started ======"));
    // gatewayTransportInit();
  }

  boolean changed = debouncer.update();
  if ( changed ) {
    checkButton();
  }
  if  ( buttonState == 1 ) {
    if ( millis() - buttonPressTimeStamp >= debouncerInterval ) {
      buttonPressTimeStamp = millis();
      aSerial.vvv().pln(F("Retriggering button"));
      if ( manualConfig == true) {
        manualConfig = false;
      }
      else {
        manualConfig = true;
        configManager(config);
      }
    }
  }

  //  if (otaSignal == 1 || resetConfig) {
  //    setReboot();
  //  }

  //  if (WiFi.status() != WL_CONNECTED && _otaSignal == 0) {
  //    ticker.attach(0.5, tick);
  //    ++wifiFailCount;
  //    if (wifiFailCount == 10) {
  //      configManager();
  //    }
  //  }
  //

  if (!_MQTT_client.connected() && WiFi.status() == WL_CONNECTED && otaSignal == 0) {
    //  _MQTT_client.disconnect();
    ticker.attach(0.3, tick);
    mqttReconnect(config);
  }

#if NTP_SERVER == 1
  if (timeStatus() != timeNotSet) {
    if (now() != prevDisplay) { //update the display only if time has changed
      prevDisplay = now();
      digitalClockDisplay();
    }
  }
#endif

  //  else if (_MQTT_client.connected() && WiFi.status() == WL_CONNECTED && otaSignal == 0) {
  //    wifiFailCount = 0;
  //    mqttFailCount = 0;
  //    ticker.detach();
  //    digitalWrite(STATE_LED, HIGH);
  //    unsigned long now = millis();
  //    if (now - lastUpdate > 30000) {
  //      Serial.printf("loop heap size: %u\n", ESP.getFreeHeap());
  //      lastUpdate = now;
  //    }
  //    //    if (now-lastYield > 20000) {
  //    //      Serial.println(F("Cleaning memory"));
  //    //      yield();
  //    //      Serial.flush();
  //    //      delay(500);
  //    //      lastYield = now;
  //    //    }
  //
  //  }
}
